Stuart front end challenge

## Available Scripts

To install all project packages and dependencies run:

### `npm install`

To run the project:

### `npm start`


## Project improvements

Regarding this project I will list a bunch of improvements:

* Improve React performace, avoid re-renders where is possible using memoazied components or functions. 
* I will add more error messages if there is error during job creation or with geocode API call.
* Write unit tests and end-to-end tests.

## Nice to have new features

This features could be a nice to have for this sample project:

* Add a list of all jobs created with information like the timestamp. This list will be retrieved every time you open the app.
* Add a route between the pick up point and drop off point. 
