import React from "react";
import ReactDOM from "react-dom";
import { StoreProvider } from "easy-peasy";

import Map from "@/components/map";
import store from "@/store";

import "@/index.css";

const App = ({ data, localeItems, locale }) => {
  return (
    <StoreProvider store={store({ data, localeItems, locale })}>
      <Map />
    </StoreProvider>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
