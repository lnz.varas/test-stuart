export default {
  geoCode: "https://stuart-frontend-challenge.now.sh/geocode/",
  saveJob: "https://stuart-frontend-challenge.now.sh/jobs/"
};
