/* eslint-disable no-param-reassign */
import { thunk, action } from "easy-peasy";
import axios from "axios";

import endpoints from "config/endpoints";

export default {
  pickIcon: "default",
  dropIcon: "default",
  pickMarker: null,
  dropMarker: null,
  isCreating: false,
  isSuccessMessage: false,
  updateIcon: action((state, payload) => {
    state[payload.type] = payload.status;
  }),
  updateMarker: action((state, payload) => {
    switch (payload.type) {
      case "pickIcon":
        state.pickMarker = payload.markerData;
        break;
      case "dropIcon":
        state.dropMarker = payload.markerData;
        break;
      default:
        state.pickMarker = null;
        state.dropMarker = null;
    }
  }),
  updateIsCreating: action((state, payload) => {
    state.isCreating = payload;
  }),
  updateIsSuccessMessage: action((state, payload) => {
    state.isSuccessMessage = payload;
  }),
  geoCode: thunk(async (actions, payload, { getStoreState }) => {
    const address = payload.value.trim();
    const state = getStoreState();
    const { type } = payload;
    if (address === "") {
      actions.updateIcon({
        type,
        status: "default"
      });
      return;
    }
    // If marker already placed skip ajax call
    if (state.geo[payload.type] === "success") {
      return;
    }
    try {
      const { data } = await axios({
        method: "post",
        url: endpoints.geoCode,
        data: {
          address
        }
      });
      actions.updateMarker({
        type,
        markerData: data
      });
      actions.updateIcon({
        type,
        status: "success"
      });
    } catch (e) {
      actions.updateIcon({
        type,
        status: "error"
      });
    }
  }),
  submitJob: thunk((actions, payload) => {
    const { resetDelay, address, successTime } = payload;
    actions.updateIsCreating(true);
    setTimeout(async () => {
      const { pickup, dropoff } = address;
      try {
        await axios({
          method: "post",
          url: endpoints.saveJob,
          data: {
            pickup,
            dropoff
          }
        });
        actions.updateMarker({
          type: ""
        });
        actions.updateIcon({
          type: "pickIcon",
          status: "default"
        });
        actions.updateIcon({
          type: "dropIcon",
          status: "default"
        });
        actions.updateIsCreating(false);
        actions.updateIsSuccessMessage(true);
        setTimeout(() => {
          actions.updateIsSuccessMessage(false);
        }, successTime);
      } catch (e) {
        // Here you can add an error message
      }
    }, resetDelay);
  })
};
