import { createStore } from "easy-peasy";

import geo from "store/models/geo";

export default () =>
  createStore({
    geo
  });
