import GoogleMapsApiLoader from "google-maps-api-loader";
import Keys from "@/config/keys";

// Map centered on paris city center by default
const mapDefault = {
  center: {
    lat: 48.8534088,
    lng: 2.3487999
  },
  zoom: 13,
  maxZoom: 16,
  streetViewControl: false,
  disableDefaultUI: true
};

const mapObject = {
  google: false,
  map: false,
  bounds: null,
  markers: []
};

export function clearMarkers() {
  // Remove previous markers
  mapObject.markers.forEach(marker => {
    marker.setMap(null);
  });
  mapObject.markers = [];
  if (mapObject.map) {
    mapObject.map.setCenter(mapDefault.center);
    mapObject.map.setZoom(mapDefault.zoom);
  }
}

export function pinMarker(markerData, icon) {
  // const marker
  const latLng = {
    lat: markerData.latitude,
    lng: markerData.longitude
  };
  const marker = new mapObject.google.maps.Marker({
    position: latLng,
    map: mapObject.map,
    icon
  });
  const markerLatLng = new mapObject.google.maps.LatLng(latLng.lat, latLng.lng);
  mapObject.bounds.extend(markerLatLng);
  marker.setAnimation(mapObject.google.maps.Animation.DROP);
  marker.setMap(mapObject.map);
  mapObject.map.fitBounds(mapObject.bounds);
  mapObject.markers.push(marker);
}

export async function initMap(mapCanvas) {
  const google = await GoogleMapsApiLoader({
    apiKey: Keys.googleMapsApiKey
  });
  mapObject.google = google;
  mapObject.map = new google.maps.Map(mapCanvas.current, mapDefault);
  mapObject.bounds = new google.maps.LatLngBounds();
}
