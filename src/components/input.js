import React from "react";
import styled from "styled-components";

const InputBox = styled.input`
  width: 100%;
  height: 32px;
  padding: 0 8px;
  border-radius: 4px;
  background: #f0f3f7;
  font-size: 1rem;
  color: #333;
  border: none;
  &::placeholder {
    color: #8596a6;
  }
`;

export default function Input({ name, placeholder, onKeyUp, onChange, value }) {
  return (
    <InputBox
      type="text"
      name={name}
      placeholder={placeholder}
      onKeyUp={onKeyUp}
      onChange={onChange}
      value={value}
    />
  );
}
