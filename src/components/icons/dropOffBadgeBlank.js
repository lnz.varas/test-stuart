import React from "react";

export default function dropOffBadgeBlank() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
    >
      <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
        <g fillRule="nonzero">
          <circle cx="16" cy="16" r="16" fill="#F0F3F7" />
          <path
            fill="#8596A6"
            d="M10 7a1 1 0 011 1v16a1 1 0 11-2 0V8a1 1 0 011-1zm2 1h11.117a.5.5 0 01.429.757l-2.237 3.729a1 1 0 000 1.029l2.237 3.728a.5.5 0 01-.43.757H12V8z"
          />
        </g>
      </g>
    </svg>
  );
}
