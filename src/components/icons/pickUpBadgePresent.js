import React from "react";

export default function pickUpBadgePresent() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
    >
      <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
        <g>
          <circle cx="16" cy="16" r="16" fill="#FFB11F" fillRule="nonzero" />
          <g transform="translate(4 4)">
            <path d="M0 0H24V24H0z" />
            <path
              fill="#FFF"
              fillRule="nonzero"
              d="M21 13v6.083c0 .507-.41.917-.917.917H4.07A1.07 1.07 0 013 18.93V13H2v-2l1-4h18l1 4v2h-1zM3.23 4h17.54a.23.23 0 01.23.23V6H3V4.23A.23.23 0 013.23 4zm2.02 10a.25.25 0 00-.25.25v3.5c0 .138.112.25.25.25h6.5a.25.25 0 00.25-.25v-3.5a.25.25 0 00-.25-.25h-6.5zm8.98 0a.23.23 0 00-.23.23V20h4v-5.77a.23.23 0 00-.23-.23h-3.54z"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
