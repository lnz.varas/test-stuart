import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import { initMap, pinMarker } from "@/utils/mapDrawer";
import AddressBox from "@/components/addressBox";
import { useStoreState } from "easy-peasy";

const PICK_MARKER_IMAGE_URL = "/public/pickUpMarker.svg";
const DROP_MARKER_IMAGE_URL = "/public/dropOffMarker.svg";

const MapContainer = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;
`;

const MapBox = styled.div`
  width: 100%;
  height: 100vh;
`;

const SuccessMessage = styled.div`
  font-size: 1rem;
  color: #fff;
  padding: 0 16px;
  height: 40px;
  line-height: 40px;
  position: absolute;
  left: 32px;
  bottom: 32px;
  border-radius: 4px;
  transform: ${props => (props.visible ? "translateY(0)" : "translateY(80px)")};
  background: rgba(51, 51, 51, 0.9);
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.4s ease-out;
`;

export default function Map() {
  const mapCanvas = useRef(null);
  const isSuccessMessage = useStoreState(state => state.geo.isSuccessMessage);
  const pickMarker = useStoreState(state => state.geo.pickMarker);
  const dropMarker = useStoreState(state => state.geo.dropMarker);

  useEffect(() => {
    initMap(mapCanvas);
  }, []);

  useEffect(() => {
    if (pickMarker !== null) {
      pinMarker(pickMarker, PICK_MARKER_IMAGE_URL);
    }
  }, [pickMarker]);

  useEffect(() => {
    if (dropMarker !== null) {
      pinMarker(dropMarker, DROP_MARKER_IMAGE_URL);
    }
  }, [dropMarker]);

  return (
    <MapContainer>
      <MapBox ref={mapCanvas} />
      <AddressBox />
      <SuccessMessage visible={isSuccessMessage}>
        Job has been created successfully!
      </SuccessMessage>
    </MapContainer>
  );
}
