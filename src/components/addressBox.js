import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useStoreState, useStoreActions } from "easy-peasy";
import { clearMarkers } from "@/utils/mapDrawer";

import Icon from "@/components/icon";
import Input from "@/components/input";
import Button from "@/components/button";

const AdressContainer = styled.div`
  position: absolute;
  top: 32px;
  left: 32px;
  width: 40%;
  min-width: 200px;
  max-width: 430px;
  background: #fff;
  border-radius: 8px;
  padding: 16px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
`;

const FormControl = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 16px;
`;

let debounceTimeout;
const DEBOUNCE_TIME = 400;
const RESET_TIME = 5 * 1000;
const SUCCESS_TIME = 4 * 1000;

export default function AddressBox() {
  const [pickAddress, setPickAddress] = useState("");
  const [dropAddress, setDropAddress] = useState("");
  const isCreating = useStoreState(state => state.geo.isCreating);
  const pickIcon = useStoreState(state => state.geo.pickIcon);
  const dropIcon = useStoreState(state => state.geo.dropIcon);
  const geoCode = useStoreActions(state => state.geo.geoCode);
  const submitJob = useStoreActions(state => state.geo.submitJob);

  const handleInput = (e, type) => {
    const { value } = e.target;
    switch (type) {
      case "pickIcon":
        setPickAddress(value);
        break;
      case "dropIcon":
        setDropAddress(value);
        break;
      default:
    }
    // Manualy debounce to avoid too many ajax calls
    clearTimeout(debounceTimeout);
    debounceTimeout = setTimeout(() => {
      geoCode({ value, type });
    }, DEBOUNCE_TIME);
  };

  const handleSubmit = () => {
    if (pickIcon !== "success" && dropIcon !== "success") {
      return;
    }
    // Submit address
    submitJob({
      address: {
        pickup: pickAddress,
        dropoff: dropAddress
      },
      resetDelay: RESET_TIME,
      successTime: SUCCESS_TIME
    });
  };

  useEffect(() => {
    if (pickIcon === "default") {
      setPickAddress("");
    }
    if (dropIcon === "default") {
      setDropAddress("");
    }
    if (pickIcon === "default" && dropIcon === "default") {
      clearMarkers();
    }
  }, [pickIcon, dropIcon]);

  return (
    <AdressContainer>
      <FormControl>
        <Icon type="pick" status={pickIcon} />
        <Input
          name="pickup"
          placeholder="Pick up address"
          onKeyUp={e => handleInput(e, "pickIcon")}
          onChange={e => handleInput(e, "pickIcon")}
          value={pickAddress}
        />
      </FormControl>
      <FormControl>
        <Icon type="drop" status={dropIcon} />
        <Input
          name="dropoff"
          placeholder="Pick up address"
          onKeyUp={e => handleInput(e, "dropIcon")}
          onChange={e => handleInput(e, "dropIcon")}
          value={dropAddress}
        />
      </FormControl>
      <Button
        label={!isCreating ? "Create Job" : "Creating..."}
        enabled={
          pickIcon === "success" && dropIcon === "success" && !isCreating
        }
        onClick={handleSubmit}
      />
    </AdressContainer>
  );
}
