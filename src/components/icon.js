import React from "react";
import styled from "styled-components";

import PickUpMarkerBlank from "@/components/icons/pickUpBadgeBlank";
import PickUpMarkerError from "@/components/icons/pickUpBadgeError";
import PickUpMarkerPresent from "@/components/icons/pickUpBadgePresent";
import DropOffMarkerBlank from "@/components/icons/dropOffBadgeBlank";
import DropOffMarkerError from "@/components/icons/dropOffBadgeError";
import DropOffMarkerPresent from "@/components/icons/dropOffBadgePresent";

const IconContainer = styled.div`
  width: 32px;
  height: 32px;
  margin-right: 8px;
`;

const iconsType = {
  pick: {
    default: <PickUpMarkerBlank />,
    error: <PickUpMarkerError />,
    success: <PickUpMarkerPresent />
  },
  drop: {
    default: <DropOffMarkerBlank />,
    error: <DropOffMarkerError />,
    success: <DropOffMarkerPresent />
  }
};

export default function Icon({ type, status }) {
  return <IconContainer>{iconsType[type][status]}</IconContainer>;
}
