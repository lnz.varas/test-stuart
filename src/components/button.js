import React from "react";
import styled from "styled-components";

const ButtonContainer = styled.button`
  width: calc(100% - 41px);
  height: 32px;
  border-radius: 4px;
  margin-left: 40px;
  text-shadow: 0 1px 2px 0 rgba(16, 162, 234, 0.3);
  background: #10a2ea;
  color: #fff;
  font-size: 1rem;
  border: none;
  opacity: ${props => (props.enabled ? 1 : 0.5)};
  cursor: ${props => (props.enabled ? "pointer" : "initial")};
`;

export default function Button({ label, enabled, onClick }) {
  return (
    <ButtonContainer enabled={enabled} onClick={onClick}>
      {label}
    </ButtonContainer>
  );
}
